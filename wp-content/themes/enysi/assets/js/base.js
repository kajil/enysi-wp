(function() {
  $(function() {
    var $doc, $win, breakpoint, divece, firstLoadSize, isIE, isIE10, isIE6, isIE7, isIE8, isIE9, maxWidth, smpUa, triggerEvent, ua, winH, winW;
    $doc = $(document);
    $win = $(window);
    winW = $win.width();
    winH = $win.height();
    ua = navigator.userAgent;
    isIE = ua.match(/msie/i);
    isIE6 = ua.match(/msie [6.]/i);
    isIE7 = ua.match(/msie [7.]/i);
    isIE8 = ua.match(/msie [8.]/i);
    isIE9 = ua.match(/msie [9.]/i);
    isIE10 = ua.match(/msie [10.]/i);
    smpUa = {
      iPhone: navigator.userAgent.indexOf('iPhone') !== -1,
      iPad: navigator.userAgent.indexOf('iPad') !== -1,
      iPod: navigator.userAgent.indexOf('iPod') !== -1,
      android: navigator.userAgent.indexOf('Android') !== -1,
      windows: navigator.userAgent.indexOf('Windows Phone') !== -1
    };
    if (smpUa.iPhone || smpUa.iPad || smpUa.android || smpUa.windows) {
      triggerEvent = "touchstart";
      divece = "other";
    } else {
      triggerEvent = "click";
      divece = "pc";
    }

    $('a.gbtn').click(function() {
      $(this).toggleClass('active');
      return $('#dnav').toggleClass('active');
    });

    $('a[href*=\\#]').not('.noscr').click(function() {
      var href, position, speed, target;
      speed = 800;
      href = $(this).attr('href');
      target = $(href === '#' || href === '' ? 'html' : href);
      position = target.offset().top;
      $('body,html').animate({
        scrollTop: position
      }, speed, 'swing');
      return false;
    });

  });

}).call(this);
