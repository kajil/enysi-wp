<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package enysi
 */

?>

<footer id="ember980" class="ember-view global"><div class="in">
    <div class="fwrap">
      <h1 class="logo"><a id="ember981" href="/" class="ember-view active">ENYSi</a></h1>
      <nav class="f-nav">



      <?php
        $args = array(
          'theme_location' => 'footer_sitemap01',
          'menu_id' => 'footer_sitemap01',
          'container' => false,
          'menu_class' => 'link01',
          'items_wrap'      => '<ul id="%1$s" class="f-link %2$s">%3$s</ul>',
        );
        wp_nav_menu($args);

        $args = array(
          'theme_location' => 'footer_sitemap02',
          'menu_id' => 'footer_sitemap02',
          'container' => false,
          'menu_class' => 'link02',
          'items_wrap'      => '<ul id="%1$s" class="f-link %2$s">%3$s</ul>',
        );
        wp_nav_menu($args);
      ?>


        <ul class="f-link link03">
          <li>
            <label for="lang" class="lang select_grp">
              <select name="lang">
                <option value="ja" selected="selected">日本語</option>
              </select>
              <span class="caret"></span>
            </label>
          </li>
          <li class="social">
            <span>FOLLOW</span>
            <a href="https://www.instagram.com/enysi_travel_lab/" class="instagram" target="_blank"><i class="fa fa-instagram"></i><span>Instagram</span></a>
            <a href="https://www.facebook.com/enysi/" class="facebook" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
          </li>
        </ul>
      </nav>
    </div>

    <nav class="link-list">
      <?php
        $args = array(
          'theme_location' => 'footer_navigation',
          'menu_id' => 'footer_navigation',
          'container' => false,
          'items_wrap'      => '<ul id="%1$s" class="link$s">%3$s</ul>',
        );
        wp_nav_menu($args);
      ?>
    </nav>

  <p class="copyright"><small>© 2017 ENYSi Ltd. All right reserved.</small></p>

</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
