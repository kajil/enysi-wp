<?php get_header(); ?>

<div id="ember755" class="ember-view wrap">
  <main>
    <div class="container">
      <div class="facilities-index">

        <section class="content">
          <div class="in">
            <header class="content-header">
              <h1>メディア一覧</h1>
            </header>

            <ul id="ember1773" class="ember-view facilities-grid">

              <?php if ( have_posts() ) : while( have_posts() ) : the_post();
                get_template_part( 'template-parts/list-stay');
              endwhile; endif; ?>

            </ul>

          </div>
        </section>

        <?php get_template_part( 'template-parts/pager'); ?>

      </div>
    </div>
  </main>
</div>

<?php
get_footer();
