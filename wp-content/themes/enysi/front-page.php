<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package enysi
 */

get_header(); ?>

<div class="ember-view wrap"><main><div class="container">

<div class="page-index">
  <section class="main ready" id="season72">
    <div class="in">
      <div class="d-tb">
        <div class="d-tbc">
          <div class="headline">
            <h1>ENYSi — いままでも、これからも、日本をたのしむ</h1>
            <p class="copy">いままでも、これからも、<br class="spShow">日本をたのしむ</p>
          </div>
        </div>
      </div>
      <!-- <p class="vertical-txt date-txt">七月十二日〜七月十六日</p> -->
      <p class="vertical-txt main-txt">
        <span id="season72-s04">冬</span>
        <span id="season72-s24">冬至</span>
        <span id="season72-s72">雪下出麦</span>
      </p>
      <p class="vertical-txt description-txt">
        <span id="season72-txt1">雪の下で、</span><br>
        <span id="season72-txt2">麦が芽を出すころ</span>
      </p>
    </div>
  </section>


  <section class="sec-content is-feature">
    <div class="sec-header">
      <h1>心に触れる旅を探す</h1>
      <p>Let's trip!</p>
    </div>
    <div class="flex-grid-container">
      <div class="flex-grid">
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_totonoeru.png');">
            <a href="/tags/fix/" class="ember-view">
              <div class="headline">
                <p>自然や歴史にふれて心をリセット</p>
                <h1>#整える</h1>
              </div>
            </a>
          </section>
        </div>
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_kutsurogu.png');">
            <a href="/tags/relax/" class="ember-view">
              <div class="headline">
                <p>やさしい香りの空間でゆっくり</p>
                <h1>#くつろぐ</h1>
              </div>
            </a>
          </section>
        </div>
      </div>
      <div class="flex-grid">
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_nomu.png');">
            <a href="/tags/drink/" class="ember-view">
              <div class="headline">
                <p>新鮮な肴で地元の味を飲み比べ</p>
                <h1>#呑む</h1>
              </div>
            </a>
          </section>
        </div>
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_mederu.png');">
            <a href="/tags/admire/" class="ember-view">
              <div class="headline">
                <p>フォトジェニックな四季を見つけて</p>
                <h1>#愛でる</h1>
              </div>
            </a>
          </section>
        </div>
      </div>
      <div class="flex-grid">
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_nostalgic.png');">
            <a href="/tags/nostalgic/" class="ember-view">
              <div class="headline">
                <p>喧騒を離れて静かな場所へ</p>
                <h1>#ノスタルジック</h1>
              </div>
            </a>
          </section>
        </div>
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_tag_touji.png');">
            <a href="/tags/hot-spring-cure/" class="ember-view">
              <div class="headline">
                <p>“湯”で“治す”と書いて「湯治」</p>
                <h1>#湯治</h1>
              </div>
            </a>
          </section>
        </div>
      </div>
    </div>
  </section>

  <section class="sec-content is-feature">
    <div class="sec-header">
      <h1>宿坊・古民家に泊まる</h1>
      <p>Beginner</p>
    </div>
    <div class="flex-grid-container">
      <div class="flex-grid">
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/beginner/shukubou/bg_enjoy.jpg');">
            <a href="/beginner/shukubou" class="ember-view">
              <div class="headline">
                <p>［静謐な時を過ごす］</p>
                <h1>はじめての宿坊</h1>
                <p class="desc">「朝のお勤め」や「座禅」など、忙しない日常から離れて自身と静かに向き合うことのできる宿泊体験。</p>
              </div>
            </a>
          </section>
        </div>
        <div class="col-6">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/beginner/kominka01/bg_enjoy.jpg');">
            <a href="/beginner/kominka" class="ember-view">
              <div class="headline">
                <p>［一棟貸しを家族で体験］</p>
                <h1>はじめての古民家</h1>
                <p class="desc">「360°ビュー」で京町家・古民家の空間を疑似体験。団体での宿泊におすすめのお宿です。</p>
              </div>
            </a>
          </section>
        </div>
      </div>
    </div>
  </section>


  <section class="sec-content is-feature">
    <div class="sec-header">
      <h1>伝統を纏う場所を訪ねる</h1>
      <p>Experience</p>
    </div>
    <div class="flex-grid-container">
      <div class="flex-grid">
        <div class="col-4">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/experience/craftsman/interviews/1/bg_main.jpg');">
            <a href="/craftsman/" class="ember-view">
              <div class="headline">
                <p>[ENYSiのオリジナル体験]</p>
                <h1>職人の仕事場を訪ねる旅</h1>
                <p class="desc">その土地の職人と対談をしながら「お客さんに喜ばれる体験」が生まれるENYSiオリジナル企画</p>
              </div>
            </a>
          </section>
        </div>
        <div class="col-4">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/experience/ichimikai/bg_honenin.jpg');">
            <a href="/ichimikai/" class="ember-view">
              <div class="headline">
                <p>[インスピレーショナルな語りの場]</p>
                <h1>一味会</h1>
                <p class="desc">社会を変革する志を抱く者が分野の垣根を越えて集まる場を「一味会」と名付けました</p>
              </div>
            </a>
          </section>
        </div>
        <div class="col-4">
          <section class="feature-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/experience/sakagura/bg_main.jpg');">
            <div>
              <div class="headline">
                <p>[酒蔵を訪ねる]</p>
                <h1>Coming Soon.</h1>
                <!-- <p class="desc">社会を変革する志を抱く者が分野の垣根を越えて集まる場を「一味会」と名付けました</p> -->
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>


  <section class="sec-content is-area-search is-feature">
    <div class="sec-header">
      <h1>エリアから探す</h1>
      <p>Area Search</p>
    </div>
    <div class="flex-grid-container">
      <div class="flex-grid">
        <div class="col-4">
          <a href="/area/kyoto/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_kyoto.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Kyoto</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/okayama/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_okayama.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Okayama</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/shiga/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_shiga.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Shiga</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/shimane/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_shimane.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Shimane</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/tokyo/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_tokyo.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Tokyo</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/nara/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_nara.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Nara</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/kanagawa/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_kanagawa.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Kanagawa</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/nagano/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_nagano.jpg');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Nagano</p>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-4">
          <a href="/area/hiroshima/" class="area-item ember-view">
            <div class="area-item-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/top/bg_area_hiroshima.png');">
              <div class="d-tb">
                <div class="d-tbc">
                  <p class="area-item-name">Hiroshima</p>
                </div>
              </div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </section>

  <section class="sec-about is-footer-about">
    <div class="sec-about-headline">
      <h1>いままでも、これからも、日本をたのしむ</h1>
      <p>ENYSi（えにし）は「心震える美しい日本」をテーマに、<br>日本ならではの体験を紹介するサービスです。</p>
      <div class="sec-about-action">
        <a href="/about" class="btn ember-view">ENYSiについて詳しく</a>
      </div>
    </div>
  </section>
</div>

</div>
</main>
</div>

<?php
get_footer();
