
    <div class="article__related flex-bottom">
    <!-- related area -->
      <div class="article__related__area area">
        <header class="area__header">
          <h1>関連記事</h1>
        </header>
        <div class="area__facilities">
          <ul id="ember2270" class="ember-view facilities-grid">
            <?php
              while (have_posts()) : the_post();
                echo '<li id="ember2271" class="ember-view facility-relate-cell">';
                echo '<a id="ember2272" href="'.get_the_permalink().'" class="ember-view">';
                echo '<figure id="facility-4" class="facility-relate-cell-figure facility-cell-kyomachiya">';


                if(get_the_post_thumbnail($post->ID)) {
                  $thumb_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumb02')[0];
                } else {
                  $thumb_image_url = get_stylesheet_directory_uri().'/assets/images/common/ogp.jpg">';
                };
                echo '<div class="facility-relate-cell-image-wrapper" style="background-image:url('.$thumb_image_url.')"><img src="'.$thumb_image_url.'"></div>';
                echo '<div class="facility-relate-cell-details">';
                if ( $terms = get_the_terms($post->ID, array('tags','area')) ) {
                  echo '<div class="facility-cell-tags">';
                    foreach ( $terms as $term ) {echo '<span class="tag tag-kind">'.esc_html($term->name).'</span>';}
                    echo '</div>';
                }
                echo '<h3 class="facility-cell-name">'.get_the_title().'</h3>';
                if(post_custom('stay_text')){echo '<p class="facility-relate-cell-prefecture">'.esc_html(post_custom('stay_text')).'</p>';}
                if(post_custom('stay_list_price')){echo '<p class="facility-relate-cell-lowest-price"><strong>'.esc_html(post_custom('stay_list_price')).'</strong>（税込）</p>';}
                echo '</div></figure></a></li>';

              endwhile;
            ?>
          </ul>
        </div>

      </div>
    </div>