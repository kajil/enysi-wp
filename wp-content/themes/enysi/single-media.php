<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @package enysi
 */
get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div class="container">
        <div id="post-<?php the_ID(); ?>" <?php post_class(array('article','article--blog')); ?>>



          <?php while ( have_posts() ) : the_post();
            get_template_part('template-parts/content-media');
          endwhile; ?>


          <?php get_sidebar(); ?>


          <?php yarpp_related( array('post_type' => 'media') ); ?>


        </div>
      </div>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer();
