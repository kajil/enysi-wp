<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package enysi
 */

get_header(); ?>

<div id="ember1096" class="ember-view wrap"><main>
  <div class="container">

<!---->
      <div class="static">
  <section class="content"><div class="in">
    <h1>News Archive</h1>

    <div class="news-list mt30" id="news">
      <div class="lst lst_news">
        <ul>
          <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php if ( post_custom('news_link')) {
                echo '<a target="_blank" href="'.esc_url(post_custom('news_link')).'" class="ember-view">';
              }else{
                echo '<a href="'.get_the_permalink().'">';
              } ?>
                <span class="date"><?php the_time('Y.n.j'); ?></span>
                <?php if(get_the_terms(  $post -> ID ,'category' )):
                    $tarms = get_the_terms(  $post -> ID ,'category' );foreach ( $tarms as $tarm ) {
                      echo '<span class="label">'.$tarm->name.'</span>';
                    }
                  endif; ?>
                <span class="text"><?php the_title(); ?></span>
              </a>
            </li>
          <?php endwhile; ?>
        </ul>
      </div>
    </div>
  </div></section>
</div>


  </div>
</main>
</div>

<?php
//get_sidebar();
get_footer();
