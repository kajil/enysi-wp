<?php get_header(); ?>


<div class="wrap">
  <main>
    <div class="container">
      <div class="facilities-index">

        <?php get_search_form(); ?>

        <section class="content">
          <div class="in">

    <?php
    //検索内容を取得
      $s = $_GET['s'];
      if(empty($s)) $s = null;

      $select_pt = $_GET['select_pt'];

      if( !empty($_GET['select_area']) ){
        $select_area = $_GET['select_area'];
        $count_genre = count($select_area);
      }else{
        $select_area = '';
        $count_area = 0;
      }


      if( !empty($_GET['select_tags']) ){
        $select_tags = $_GET['select_tags'];
        $count_genre = count($select_tags);
      }else{
        $select_tags = '';
        $count_tags = 0;
      }

      $tag_count = $count_area + $count_tags;

      $price_min = $_GET['price_min'];
      $price_max = $_GET['price_max'];

    ?>


    <?php if( empty($s) && empty($select_area) && empty($select_tags) && ($price_min == 1000) &&($price_max == 50000) ):
    echo '<h3 class="ta_c fs20">検索条件を指定してください</h3>';
    else: //条件未指定?>

      <div class="search_label_box mb40 sm_mb20">
        <h1 class="bold mr10">検索内容</h1>

        <?php
          if($s) {
            echo '<p>キーワード: '.$s.'</p>';
          }
          echo '<div class="facility-cell-tags">';
          if (is_array($select_area)) {
            foreach($select_area as $val){
              echo '<span class="tag">'.get_term_by('slug',$val,"area")->name.'</span>';
            }
          }

          if (is_array($select_tags)) {
            foreach($select_tags as $val){
              echo '<span class="tag">'.get_term_by('slug',$val,"tags")->name.'</span>';
            }
          }

          if($tag_count > 1) {echo '<span>のいずれかに該当</span>';}else{echo '<span>に該当</span>';}

          echo '</div>';

          if( !(($price_min == 1000) && ($price_max == 50000)) )
            echo '<p>価格:　'.$price_min.' 〜 '.$price_max.'</p>';

        ?>

      </div>

      <?php

      if(!$select_pt){//ポストタイプの選択がないときはすべて
        $select_pt = array('stay','media');
      }


        if($select_tags){
          $taxquerysp[] = array(
            'taxonomy'=>'tags',
            'terms'=> $select_tags,
            'field'=>'slug',
            'operator'=>'IN'
          );
        }
        if($select_area){
          $taxquerysp[] = array(
            'taxonomy'=>'area',
            'terms'=> $select_area,
            'field'=>'slug',
            'operator'=>'IN'
          );
        }
        $taxquerysp['relation'] = 'OR';




        $metaquerysp[] = array(
          // 'relation' => 'OR',
          // array(
          //   'key' => 'low',
          //   'compare'=>'NOT EXISTS',
          // ),
          // array(
            'relation' => 'AND',
            array(
              'key'=>'high',
              'value'=> $price_min,
              'type'=>'numeric',
              'compare'=>'>',
            ),
            array(
              'key'=>'low',
              'value'=> $price_max,
              'type'=>'numeric',
              'compare'=>'<',
            ),
//          ),
        );

        if( ($price_min == 1000) && ($price_max == 50000) ) $metaquerysp = null;


        query_posts( array(
          's' => $s,
          'posts_per_page' => 20,
          'paged' => $paged,
          'post_type' => $select_pt,
          'tax_query' => $taxquerysp,
          'meta_query' => $metaquerysp,
          'orderby' => 'meta_value',
          'order'=> 'ASC',
        ) );



          echo '<ul class="facilities-grid">';
          if ( have_posts() ) : while( have_posts() ) : the_post();
        ?>

<?php /*
        <li><a href="<?php the_permalink(); ?>" style="border:1px solid;margin: 30px;padding: 10px; display:block;">
        <?php if( $terms = get_the_terms($post->ID, array('tags','area')) ) {
            echo '<div class="facility-cell-tags">';
          foreach ( $terms as $term ) {echo '<span class="tag ember-view" href="'.get_term_link($term).'">'.esc_html($term->name).'</span>';}
            echo '</div>';
          }
          the_title();
          $rep_group = SCF::get( 'stay_price_group' ); if( $rep_group[0]['stay_price_name']): $price_high = $price_row = null;    foreach ( $rep_group as $fields ) :$price = intval( preg_replace('/[^0-9]/', '', $fields['stay_price_num']) );      if(!$price_high){        $price_high = $price; $price_row = $price;      }      if($price_high < $price) $price_high = $price;      if($price_row > $price) $price_row = $price;    endforeach;    if($price_high == $price_row): $price_text = '<strong>'.number_format($price_high).'円</strong>（税抜）';    else: $price_text = '<strong>'.number_format($price_row).'円〜'.number_format($price_high).'円</strong>（税抜）';    endif;  endif;
          $check = SCF::get( 'stay_price_group' ); if($check[0]){echo '<p>'.$price_text.'</p>';}
          if(post_custom('low')) echo '<p>'.post_custom('low').' 〜 '.post_custom('high').'</p>';
          ?>
          </a></li>
*/ ?>


<?php



                get_template_part( 'template-parts/list-item');


          endwhile; endif;
          echo '</ul>';

      ?>

    </div>

    <?php
      endif;//条件未指定

      if(!empty($cpt)){
        $cpt_object = get_post_type_object($cpt);
        echo '<div class="ta_c pt30"><a class="open_button" href="'.get_post_type_archive_link($cpt).'">'.$cpt_object->labels->singular_name.' 一覧にもどる</a></div>';
      }
    ?>




  </section>

  <?php get_template_part( 'template-parts/pager'); ?>

  </div></div></main></div>

<?php get_footer(); ?>