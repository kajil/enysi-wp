<?php get_header(); ?>

<div id="ember740" class="ember-view wrap">
  <main>
    <div class="container">

      <div class="static">
        <section class="content">
          <div class="in">

            <?php while ( have_posts() ) : the_post(); ?>

              <div class="news-detail">
                <h1><?php the_title(); ?></h1>
                <p class="date"><?php the_time('Y.n.j'); ?></p>
                <?php the_content(); ?>
              </div>

            <?php endwhile; ?>

            <p class="mt15"><a id="ember1019" href="/news" class="ember-view active">News Archive</a></p>
          </div>
        </section>
      </div>

    </div>
  </main>
</div>

<?php
//get_sidebar();
get_footer();
