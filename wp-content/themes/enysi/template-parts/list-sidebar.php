<?php
  if( has_post_thumbnail()) {
    $image_id = get_post_thumbnail_id();
    $image_url = wp_get_attachment_image_src($image_id, 'author_thumb',false)[0];
  }
  $title = esc_html( get_the_title() );
  $link = esc_url(get_the_permalink());
  $exerpt = get_the_excerpt();
  $date = get_the_time('Y.m.d');
  global $i;

  echo '<div class="ranking__list__cell"><a href="'.$link.'">';

  echo '<figure class="photo"><img src="'.$image_url.'" alt="'.$title.'"></figure>';
  if($i){echo '<span class="ranknum ranknum--1">'.$i.'</span>';}
  echo '<p class="name">'.$title.'</p>';
  echo '<p class="text">'.$exerpt.'</p>';
  // if ($terms = get_the_terms($post_id, 'post_tag')) {
  //   echo '<p class="tags">';
  //   foreach ( $terms as $term ) {echo '<span class="tag tag--gray">'.esc_html($term->name).'</span>';}
  //   echo '</p>';
  // }
  echo '<p class="date">'.$date.'</p>';

  echo '</a></div>';
