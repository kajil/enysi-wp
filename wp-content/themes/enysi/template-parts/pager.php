<ul id="ember1873" class="ember-view pager-nav list-inline list-inline-center">
  <?php global $wp_rewrite;
  $paginate_base = get_pagenum_link(1);
  if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
    $paginate_format = '';
    $paginate_base = add_query_arg('paged','%#%');
  }
  else{
    $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
    user_trailingslashit('page/%#%/','paged');;
    $paginate_base .= '%_%';
  }
  echo paginate_links(array(
    'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 2,
    'current' => ($paged ? $paged : 1),
    'before_page_number' => '<span class="btn btn-narrow btn-normal ember-view active">',
    'after_page_number' => '</span>',
    'type' => 'list',
    'prev_text' => '<span class="btn btn-narrow btn-normal btn-disabled ember-view disabled"><i class="fa fa-angle-left"></i></span>',
    'next_text' => '<span class="btn btn-narrow btn-normal ember-view"><i class="fa fa-angle-right"></i></span>',
  )); ?>
</ul>
