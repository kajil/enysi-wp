<?php
  //if( is_single() && !is_user_logged_in() && !isBot() ) //個別記事 かつ ログインしていない かつ 非ボット
  set_post_views(); //アクセスをカウントする
 ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<div id="ember755" class="ember-view wrap"><main>
  <div class="container">

<!---->
      <div class="article article--shukubou">
  <div class="article__parts flex-right">

    <!-- instagram images -->
    <?php if ( post_custom('stay_main_instagram')) :
      echo '<div class="article__parts__slider _sliders"><div class="_sliders_child">';
      echo post_custom('stay_main_instagram');
      echo '</div></div>';
      endif; ?>

    <!-- reservation button -->
      <?php
        $rep_group = SCF::get( 'stay_price_group' );//リピートグループを代入
        if( $rep_group[0]['stay_price_name'])://グループ内１つ目の任意のフィールドに値があるか
          echo '<div class="article__parts__reservation">';
          foreach ( $rep_group as $fields ) ://グループを繰り返し処理
            echo '<div class="reservation-block"><p class="reservation-block__txt">';
            echo '<span class="type">'.$fields['stay_price_name'].'</span>';
            echo '<span class="price">¥'.$fields['stay_price_num'].'(税込)</span>';
            echo '</p><p class="reservation-block__btn">';
            echo '<a href="'.esc_url($fields['stay_price_url']).'" class="btn">'.$fields['stay_price_button_label'].'<span class="spShow">（¥'.$fields['stay_price_num'].'〜）</span></a>';
            echo '</p></div>';
          endforeach;
          echo '<p class="note"><a href="#" class="link">宿泊,体験のリクエストとは？</a></p>';
          echo '</div>';
        endif;
      ?>

  </div>

  <div class="article__facilities flex-left">

    <!-- facilities overview -->
    <div class="article__facilities__overview overview">
      <header class="overview__header">
        <h1><?php the_title(); ?></h1>
        <?php
          if ( $terms = get_the_terms($post->ID, array('tags','area')) ) {
            echo '<p class="tags">';
          foreach ( $terms as $term ) {echo '<span class="tag tag--gray">'.esc_html($term->name).'</span>';}
            echo '</p>';
          }
        ?>
        <p class="ruby"><?php echo esc_html(post_custom('stay_en')); ?></p>
      </header>
      <div class="overview__description">
        <p><?php echo nl2br(esc_html(post_custom('stay_text'))); ?></p>

        <!-- share button -->
        <div class="article__share">
          <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>

      </div>

      <div class="overview__info">
        <ul class="list list--imgicons">
          <?php
            $rep_group = SCF::get( 'stay_feature_group' );//リピートグループを代入
            if( $rep_group[0]['stay_feature_title'])://グループ内１つ目の任意のフィールドに値があるか
              foreach ( $rep_group as $fields ) ://グループを繰り返し処理
                if($fields['stay_feature_question']){
                  echo '<li><i class="fa '.esc_attr($fields['stay_feature_icon']).'" aria-hidden="true"></i>'.esc_html($fields['stay_feature_title']).'：<a href="'.esc_url($fields['stay_feature_question']).'">'.esc_html($fields['stay_feature_text']).'</a><i class="fa fa-question-circle" aria-hidden="true"></i></li>';
                }else{
                  echo '<li><i class="fa '.esc_attr($fields['stay_feature_icon']).'" aria-hidden="true"></i>'.esc_html($fields['stay_feature_title']).'：'.esc_html($fields['stay_feature_text']).'</li>';
                }
              endforeach;
            endif;
            if (post_custom('stay_address')) {echo '<li class="area"><i class="fa fa-map-marker" aria-hidden="true"></i>'.esc_html(post_custom('stay_address')).' <a href="#" class="link">マップを見る</a></li>';}
          ?>
        </ul>
          <?php if ( post_custom('stay_author_image')) {
            $author_image_url = wp_get_attachment_image_src(post_custom('stay_author_image'), 'author_thumb',false)[0];
            echo '<figure class="photo"><img src="'.$author_image_url.'" style="width:100px;height:100px;"></figure>';
          } ?>
      </div>

      <?php if ( post_custom('stay_staff_comment')) {
        echo '<div class="overview__enysicomment enysicomment"><h2>ENYSiスタッフコメント</h2>';
        echo '<p>'.wp_kses_post(post_custom('stay_staff_comment')).'</p>';
        echo '</div></div>';
      } ?>



      <?php
      /*facilities experience*/
      if ( post_custom('stay_plan01_title')) :
        $stay_plan_group_num = count( SCF::get( 'stay_plan_group01' ) );//子タームの数をカウント
        echo '<div class="article__facilities__experience experience"><header class="experience__header">';
        echo '<h1>'.esc_html(post_custom('stay_plan01_title')).'<span>('.$stay_plan_group_num.'件)</span></h1>';
        echo '</header>';

        $rep_group = SCF::get( 'stay_plan_group01' );//リピートグループを代入
        if( $rep_group[0]['stay_plan01_name'])://グループ内１つ目の任意のフィールドに値があるか
          foreach ( $rep_group as $fields ) ://グループを繰り返し処理

            echo '<div class="_sliders"><div class="experience__box _sliders_child">';
            echo '<h2>';
            foreach ( $fields['stay_plan01_label'] as $label ) ://グループを繰り返し処理
              echo '<span class="tag tag--blue">'.$label.'</span>';
            endforeach;
            if($fields['stay_plan01_question']){
              echo '<a href="'.esc_url($fields['stay_plan01_question']).'">'.$fields['stay_plan01_name'].'<i class="fa fa-question-circle" aria-hidden="true"></i></a>';
            }else{
              echo $fields['stay_plan01_name'];
            }
            echo '</h2>';

            if ( post_custom('stay_plan01_image')) {
              $image_url = wp_get_attachment_image_src($fields['stay_plan01_image'], 'thumb01',false)[0];
              echo '<figure><img src="'.$image_url.'" style="width:430px;height:auto;"></figure>';
            }

            if ( post_custom('stay_plan01_text')) {echo '<p class="description">'.nl2br(wp_kses_post($fields['stay_plan01_text'])).'</p>';}

            if ( post_custom('stay_plan01_spec01')) {echo '<div class="description">'.nl2br(wp_kses_post($fields['stay_plan01_spec01'])).'</div>';}

            if ( post_custom('stay_plan01_reservation_url')) {echo '<p class="showmore"><a href="'.esc_url($fields['stay_plan01_reservation_url']).'" class="link">詳細を見る<i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>';}

            if ( post_custom('stay_plan01_spec02')) {echo '<div class="description">'.nl2br(wp_kses_post($fields['stay_plan01_spec02'])).'</div>';}

            echo '</div>';

            if($stay_plan_group_num > 1){
              echo '<nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
              <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>';
            }

            echo '</div>';

          endforeach;
          endif;

          echo '</div>';
        endif;
      ?>



      <?php
        /*facilities experience*/
        if ( post_custom('stay_plan02_title')) :
          $stay_plan_group_num = count( SCF::get( 'stay_plan_group02' ) );//子タームの数をカウント
          echo '<div class="article__facilities__experience experience"><header class="experience__header">';
          echo '<h1>'.esc_html(post_custom('stay_plan02_title')).'<span>('.$stay_plan_group_num.'件)</span></h1>';
          echo '</header>';

          $rep_group = SCF::get( 'stay_plan_group02' );//リピートグループを代入
          if( $rep_group[0]['stay_plan02_name'])://グループ内１つ目の任意のフィールドに値があるか
            foreach ( $rep_group as $fields ) ://グループを繰り返し処理

              echo '<div class="_sliders"><div class="experience__box _sliders_child">';
              echo '<h2>';
              foreach ( $fields['stay_plan02_label'] as $label ) ://グループを繰り返し処理
                echo '<span class="tag tag--blue">'.$label.'</span>';
              endforeach;
              if($fields['stay_plan02_question']){
                echo '<a href="'.esc_url($fields['stay_plan02_question']).'">'.$fields['stay_plan02_name'].'<i class="fa fa-question-circle" aria-hidden="true"></i></a>';
              }else{
                echo $fields['stay_plan02_name'];
              }
              echo '</h2>';

              if ( post_custom('stay_plan02_image')) {
                $image_url = wp_get_attachment_image_src($fields['stay_plan02_image'], 'thumb02',false)[0];
                echo '<figure><img src="'.$image_url.'" style="width:430px;height:auto;"></figure>';
              }

              if ( post_custom('stay_plan02_text')) {echo '<p class="description">'.nl2br(wp_kses_post($fields['stay_plan02_text'])).'</p>';}

              if ( post_custom('stay_plan02_spec02')) {echo '<div class="description">'.nl2br(wp_kses_post($fields['stay_plan02_spec01'])).'</div>';}

              if ( post_custom('stay_plan02_reservation_url')) {echo '<p class="showmore"><a href="'.esc_url($fields['stay_plan02_reservation_url']).'" class="link">詳細を見る<i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>';}

              if ( post_custom('stay_plan02_spec02')) {echo '<div class="description">'.nl2br(wp_kses_post($fields['stay_plan02_spec02'])).'</div>';}

              echo '</div>';

              if($stay_plan_group_num > 1){
                echo '<nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
                <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>';
              }

              echo '</div>';

            endforeach;
          endif;

          echo '</div>';
        endif;
      ?>



    <!-- facilities info -->
    <div class="article__facilities__info info">
      <header class="info__header">
        <h1>施設情報</h1>
      </header>
      <div class="info__table">
        <table>
          <tr>
            <th>アメニティ<br>/設備</th>
            <td>
          <?php
            $rep_group = SCF::get( 'stay_info_group' );//リピートグループを代入
            if( $rep_group[0]['stay_info_title'])://グループ内１つ目の任意のフィールドに値があるか
              echo '<ul class="list list--amenity">';
              foreach ( $rep_group as $fields ) ://グループを繰り返し処理
                if('deactive' ==$fields['stay_info_active']) {echo '<li class="disabled">';}else{echo '<li>';}
                echo '<i class="fa '.esc_attr($fields['stay_info_icon']).'" aria-hidden="true"></i>'.esc_html($fields['stay_info_title']).'</li>';
              endforeach;
              echo '</ul>';
            endif;

            $rep_group = SCF::get( 'stay_info_group_second' );//リピートグループを代入
            if( $rep_group[0]['stay_info2_title'])://グループ内１つ目の任意のフィールドに値があるか
              echo '<p class="shomore"><a href="#" class="link">もっと見る</a></p><ul class="list list--amenity">';
              foreach ( $rep_group as $fields ) ://グループを繰り返し処理
                if('deactive' ==$fields['stay_info2_active']) {echo '<li class="disabled">';}else{echo '<li>';}
                echo '<i class="fa '.esc_attr($fields['stay_info2_icon']).'" aria-hidden="true"></i>'.esc_html($fields['stay_info2_title']).'</li>';
              endforeach;
              echo '</ul>';
            endif;
          ?>
            </td>
          </tr>

          <?php
            $rep_group = SCF::get( 'stay_description_group' );//リピートグループを代入
            if( $rep_group[0]['stay_description_title'])://グループ内１つ目の任意のフィールドに値があるか
              foreach ( $rep_group as $fields ) ://グループを繰り返し処理
                echo '<tr><th>'.wp_kses_post($fields['stay_description_title']).'</th><td>'.nl2br(wp_kses_post($fields['stay_description_text'])).'</td></tr>';
              endforeach;
            endif;
          ?>

        </table>
      </div>
    </div>
  </div>



  <div class="article__access flex-bottom">

    <!-- access map -->
    <?php if ( post_custom('stay_address')) : ?>
      <div class="article__access__map map">
        <header class="map__header">
          <h1>アクセス</h1>
        </header>
        <div class="map__map">
          <div id="googlemap">
            <?php
              if(post_custom('stay_longitude') && post_custom('stay_latitude')){
                echo do_shortcode('[map width="540px" height="380px" lat="'.post_custom('stay_latitude').'" lng="'.post_custom('stay_longitude').'"]');
              }else{
                echo do_shortcode('[map width="540px" height="380px" addr="'.post_custom('stay_address').'"]');
              }
            ?>
          </div>
          <?php echo '<p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i>'.esc_html(post_custom('stay_address')).' <a target="_blank" href="https://www.google.co.jp/maps?q='.post_custom('stay_address').'" class="link">googleマップを見る</a>'; ?>
        </div>
      </div>
    <?php endif; ?>

    <!-- access area information -->
    <?php
      $rep_group = SCF::get( 'stay_around_group' );//リピートグループを代入
      if( $rep_group[0]['stay_around_name'])://グループ内１つ目の任意のフィールドに値があるか
        echo '
          <div class="article__access__area area">
            <header class="area__header"><h1>周辺情報</h1></header>
            <div class="area__enysicomment enysicomment">
              <h2>ENYSiオススメ</h2><ul class="list list--imgicons">';
        foreach ( $rep_group as $fields ) ://グループを繰り返し処理
          echo '<li><a href="'.esc_url($fields['stay_around_url']).'"><i class="fa '.esc_attr($fields['stay_around_icon']).'" aria-hidden="true"></i>'.esc_html($fields['stay_around_name']).'</a></li>';
        endforeach;
        echo '</ul></div></div>';
      endif;
     ?>

  </div>

  <div class="article__review flex-bottom">



    <?php
      /* review sns */
      $rep_group = SCF::get( 'insta_list_group' );//リピートグループを代入
      $rep_group_num = count( $rep_group );
      if( $rep_group[0]['insta_list'])://グループ内１つ目の任意のフィールドに値があるか
        echo '<div class="article__review flex-bottom"><div class="article__review__sns"><ul class="list list--snsreview _sliders">';
        foreach ( $rep_group as $fields ) ://グループを繰り返し処理
          echo '<li class="_slide_child"><iframe class="instagram-media instagram-media-rendered" id="instagram-embed-1" src="https://www.instagram.com/p/'.esc_html($fields['insta_list']).'/embed/captioned/?cr=1&amp;v=7#%7B%22ci%22%3A1%2C%22os%22%3A3100.3250000000003%7D" allowtransparency="true" frameborder="0" height="456" data-instgrm-payload-id="instagram-media-payload-1" scrolling="no" style="background: rgb(255, 255, 255); border: 1px solid rgb(219, 219, 219); margin: 1px 1px 12px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px; box-shadow: none; display: block; padding: 0px;"></iframe></li>';
        endforeach;

        if($rep_group_num > 1){
          echo '<nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
          <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>';
        }

        echo '</ul></div></div>';
      endif;
    ?>



<?php
  $terms = get_the_terms($post, 'area');
  $term_id = $terms[0]->term_id;
  $args = array(
    'post_type' => 'stay', //投稿タイプは制作事例なのでworksに設定
    'posts_per_page' => 4, //出力する記事の数
    'orderby' => 'rand',
    'post__not_in' => array($post->ID),
    'tax_query' => array(
      array(
        'taxonomy' => 'area', //タクソノミー名
        'field' => 'id',
        'terms' => $term_id //タームのスラッグ
      )
    )
  );
  $postslist = get_posts($args);
  if($postslist):
?>
      <div class="article__related flex-bottom">
        <div class="article__related__area area">
          <header class="area__header"><h1>周辺の施設</h1></header>
          <div class="area__facilities">
            <ul id="ember2270" class="ember-view facilities-grid">
              <?php foreach ($postslist as $post) : setup_postdata($post);//子タームに属している記事のループstart
                echo '<li id="ember2271" class="ember-view facility-relate-cell">';
                echo '<a id="ember2272" href="'.get_the_permalink().'" class="ember-view">';
                echo '<figure id="facility-4" class="facility-relate-cell-figure facility-cell-kyomachiya">';


                if(get_the_post_thumbnail($post->ID)) {
                  $thumb_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumb02')[0];
                } else {
                  $thumb_image_url = get_stylesheet_directory_uri().'/assets/images/common/ogp.jpg">';
                };
                echo '<div class="facility-relate-cell-image-wrapper" style="background-image:url('.$thumb_image_url.')"><img src="'.$thumb_image_url.'"></div>';
                echo '<div class="facility-relate-cell-details">';
                if ( $terms = get_the_terms($post->ID, array('tags','area') )) {
                  echo '<div class="facility-cell-tags">';
                    foreach ( $terms as $term ) {echo '<span class="tag tag-kind">'.esc_html($term->name).'</span>';}
                    echo '</div>';
                }
                echo '<h3 class="facility-cell-name">'.get_the_title().'</h3>';
                if(post_custom('stay_text')){echo '<p class="facility-relate-cell-prefecture">'.esc_html(post_custom('stay_text')).'</p>';}
                if(post_custom('stay_list_price')){echo '<p class="facility-relate-cell-lowest-price"><strong>'.esc_html(post_custom('stay_list_price')).'</strong>（税込）</p>';}
                echo '</div></figure></a></li>';

    endforeach;
?>
      </ul></div></div></div>
<?php endif; ?>


</div>


  </div>
</main>
</div>





</article><!-- #post-## -->
