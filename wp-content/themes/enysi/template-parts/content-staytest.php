<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    <?php the_content(); ?>
  </div><!-- .entry-content -->




<div id="ember755" class="ember-view wrap"><main>
  <div class="container">

<!---->
      <div class="article article--shukubou">
  <div class="article__parts flex-right">

    <!-- instagram images -->
    <div class="article__parts__slider _sliders">
      <div class="_sliders_child">
        <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-0" src="https://www.instagram.com/p/BI6AS0_htGt/embed/?cr=1&amp;v=7#%7B%22ci%22%3A0%2C%22os%22%3A3096.55%7D" allowtransparency="true" frameborder="0" height="452" data-instgrm-payload-id="instagram-media-payload-0" scrolling="no" style="background: rgb(255, 255, 255); border: 1px solid rgb(219, 219, 219); margin: 1px 1px 12px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px; box-shadow: none; display: block; padding: 0px;"></iframe>
      </div>
    </div>

    <!-- reservation button -->
    <div class="article__parts__reservation">
      <div class="reservation-block">
        <p class="reservation-block__txt">
          <span class="type">日帰り体験</span>
          <span class="price">¥2,160(税込)</span>
        </p>
        <p class="reservation-block__btn">
          <a href="#" class="btn">体験リクエスト<span class="spShow">（¥2,160〜）</span></a>
        </p>
      </div>
      <div class="reservation-block">
        <p class="reservation-block__txt">
          <span class="type">宿泊体験</span>
          <span class="price">¥18,000(税込)</span>
        </p>
        <p class="reservation-block__btn">
          <a href="#" class="btn">宿泊リクエスト<span class="spShow">（¥18,000〜）</span></a>
        </p>
      </div>
      <p class="note"><a href="#" class="link">宿泊,体験のリクエストとは？</a></p>
    </div>
  </div>

  <div class="article__facilities flex-left">

    <!-- facilities overview -->
    <div class="article__facilities__overview overview">
      <header class="overview__header">
        <h1>高野山 一乗院</h1>
        <p class="tags"><span class="tag tag--gray">宿坊</span></p>
        <p class="ruby">Koyasan Ichijo-in</p>
      </header>
      <div class="overview__description">
        <p>江戸の世に、名室と定められた高野山の宿坊</p>

        <!-- share button -->
        <div class="article__share">
          <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>

      </div>


      <div class="overview__info">
        <ul class="list list--imgicons">
          <li><i class="fa fa-user" aria-hidden="true"></i>住職：佐伯公応</li>
          <li><i class="fa fa-book" aria-hidden="true"></i>宗派：<a href="#">曹洞宗</a><i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li><i class="fa fa-home" aria-hidden="true"></i>宿泊：<a href="#">寺院内 和室タイプ</a><i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li><i class="fa fa-comments" aria-hidden="true"></i>言語：日本語、英語</li>
          <li class="area"><i class="fa fa-map-marker" aria-hidden="true"></i>和歌山県伊都郡高野町高野山606 <a href="#" class="link">マップを見る</a></li>
        </ul>
        <figure class="photo"><img src="https://dummyimage.com/100x100/a3a3a3/ffffff.png" alt=""></figure>
      </div>
      <div class="overview__enysicomment enysicomment">
        <h2>ENYSiスタッフコメント</h2>
        <p>「世界遺産」として、観光名所としても有名な寺院。本堂にて行われる朝のお勤めはとても荘厳な雰囲気です。</p>
      </div>
    </div>

    <!-- facilities experience  -->
    <div class="article__facilities__experience experience">
      <header class="experience__header">
        <h1>日帰りで出来る体験<span>(4件)</span></h1>
      </header>
      <div class="_sliders">
        <div class="experience__box _sliders_child">
          <h2><span class="tag tag--blue">オススメ</span><a href="#">阿字観</a><i class="fa fa-question-circle" aria-hidden="true"></i></h2>
          <figure><img src="https://dummyimage.com/430x260/a3a3a3/ffffff.png" alt=""></figure>
          <p class="description">国内最大級の石庭、蟠龍庭の中に建つ阿字観道場（一般非公開）にて、”阿字観”を体験できます。</p>
          <ul class="list list--disc">
            <li>・開催日　：毎週4日開催（金・土・日・月）</li>
            <li>・開催時間：1日4回実施（9:30、11:30、13:30、15:30）</li>
            <li>・所要時間：約1時間程度</li>
            <li>・参加費　：<strong>1,000円/人（※ 宿泊者は無料）</strong><br>（拝観窓口にて、別途内拝料をお支払い下さい。）</li>
          </ul>
          <p class="showmore"><a href="#" class="link">詳細を見る<i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
        </div>
        <nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
        <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>
      </div>
    </div>

    <div class="article__facilities__experience experience">
      <header class="experience__header">
        <h1>宿泊ができる部屋<span>(4件)</span></h1>
      </header>
      <div class="_sliders">
        <div class="experience__box _sliders_child">
          <h2><span class="tag tag--blue">オススメ</span><a href="#">石楠花の間</a><i class="fa fa-question-circle" aria-hidden="true"></i></h2>
          <figure><img src="https://dummyimage.com/430x260/a3a3a3/ffffff.png" alt=""></figure>
          <p class="description">ゆとりある10畳の和室に、開放感を演出する大きな窓から、緑豊かな庭をご覧いただけるお部屋。</p>
          <ul class="list list--disc">
            <li>・料金　：<strong>29,630（税込）/人</strong></li>
            <li>・人数　：2名〜 8名</li>
            <li>・食事　：2食付き（夕食・朝食）</li>
            <li>・風呂　：共同大浴場</li>
            <li>・トイレ：部屋内にあり</li>
            <li>・予約　：1日前までにご予約下さい</li>
          </ul>
          <p class="showmore"><a href="#" class="link">詳細を見る<i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
        </div>
        <nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
        <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>
      </div>
    </div>

    <!-- facilities info -->
    <div class="article__facilities__info info">
      <header class="info__header">
        <h1>施設情報</h1>
      </header>
      <div class="info__table">
        <table>
          <tr>
            <th>施設紹介</th>
            <td>
              <p>建立：弘仁年間、善化上人により一乗院の開基。（古い記録は焼失し、詳細は不明）<a href="#" class="link">もっと見る</a></p>
              <p>宝物：金剛界曼荼羅など、数多くの宝物見学が可能。<a href="#" class="link">もっと見る</a></p>
              <p>行事：1年を通して善光寺では様々な行事が行われます。<a href="#" class="link">もっと見る</a></p>
            </td>
          </tr>
          <tr>
            <th>アメニティ<br>/設備</th>
            <td>
              <ul class="list list--amenity">
                <li><i class="fa fa-sticky-note-o" aria-hidden="true"></i>タオル</li>
                <li class="disabled">バスタオル</li>
                <li class="disabled">浴衣</li>
                <li>歯ブラシ</li>
                <li>電源</li>
                <li><i class="fa fa-wifi" aria-hidden="true"></i>無料wifi</li>
                <li class="disabled"><i class="fa fa-television" aria-hidden="true"></i>テレビ</li>
                <li><i class="fa fa-thermometer-full" aria-hidden="true"></i>エアコン</li>
              </ul>
              <p class="shomore"><a href="#" class="link">もっと見る</a></p>
            </td>
          </tr>
          <tr>
            <th>キャンセル<br>ポリシー</th>
            <td>
              <p>3日前 0時まで：30%<br>前日の0時まで：50%<br>当日・連絡なしの不泊：100％</p>
              <p>※3日前0時まではキャンセル料はかかりません。</p>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>

  <div class="article__access flex-bottom">

    <!-- access map -->
    <div class="article__access__map map">
      <header class="map__header">
        <h1>アクセス</h1>
      </header>
      <div class="map__map">
        <div id="googlemap">
          <img src="https://dummyimage.com/540x380/a3a3a3/ffffff.png" alt="">
        </div>
        <p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i>和歌山県伊都郡高野町高野山606 <a href="#" class="link">googleマップを見る</a></p>
      </div>
    </div>

    <!-- access area information -->
    <div class="article__access__area area">
      <header class="area__header">
        <h1>周辺情報</h1>
      </header>
      <div class="area__enysicomment enysicomment">
        <h2>ENYSiオススメ</h2>
        <ul class="list list--imgicons">
          <li><a href="#"><i class="fa fa-cutlery" aria-hidden="true"></i>会席料理屋「ENYSi処」</a></li>
          <li><a href="#"><i class="fa fa-cutlery" aria-hidden="true"></i>カフェ「ENYSi茶屋 - 愛宕神社」</a></li>
          <li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>奥の院</a></li>
          <li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>高野山「福知院」</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="article__review flex-bottom">

    <!-- review sns -->
    <div class="article__review__sns">
      <ul class="list list--snsreview _sliders">
        <li class="_slide_child">
          <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-1" src="https://www.instagram.com/p/BQhR0LZgdhm/embed/captioned/?cr=1&amp;v=7#%7B%22ci%22%3A1%2C%22os%22%3A3100.3250000000003%7D" allowtransparency="true" frameborder="0" height="456" data-instgrm-payload-id="instagram-media-payload-1" scrolling="no" style="background: rgb(255, 255, 255); border: 1px solid rgb(219, 219, 219); margin: 1px 1px 12px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px; box-shadow: none; display: block; padding: 0px;"></iframe>
        </li>
        <li class="_slide_child">
          <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-2" src="https://www.instagram.com/p/BQhR0LZgdhm/embed/captioned/?cr=1&amp;v=7#%7B%22ci%22%3A2%2C%22os%22%3A3102.6800000000003%7D" allowtransparency="true" frameborder="0" height="456" data-instgrm-payload-id="instagram-media-payload-2" scrolling="no" style="background: rgb(255, 255, 255); border: 1px solid rgb(219, 219, 219); margin: 1px 1px 12px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px; box-shadow: none; display: block; padding: 0px;"></iframe>
        </li>
        <li class="_slide_child">
          <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-3" src="https://www.instagram.com/p/BQhR0LZgdhm/embed/captioned/?cr=1&amp;v=7#%7B%22ci%22%3A3%2C%22os%22%3A4151.37%7D" allowtransparency="true" frameborder="0" height="456" data-instgrm-payload-id="instagram-media-payload-3" scrolling="no" style="background: rgb(255, 255, 255); border: 1px solid rgb(219, 219, 219); margin: 1px 1px 12px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px; box-shadow: none; display: block; padding: 0px;"></iframe>
        </li>
        <nav class="slide_prev"><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></nav>
        <nav class="slide_next"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></nav>
      </ul>
    </div>
  </div>

  <div class="article__related flex-bottom">
    <!-- related area -->
    <div class="article__related__area area">
      <header class="area__header">
        <h1>周辺の施設</h1>
      </header>
      <div class="area__facilities">
        <ul id="ember2270" class="ember-view facilities-grid">
          <li id="ember2271" class="ember-view facility-relate-cell">
            <a id="ember2272" href="/facilities/4" class="ember-view">
              <figure id="facility-4" class="facility-relate-cell-figure facility-cell-kyomachiya">
                <div class="facility-relate-cell-image-wrapper" style="background-image:url(https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/4/large_file.jpg)">
                  <img src="https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/4/large_file.jpg">
                </div>
                <div class="facility-relate-cell-details">
                  <div class="facility-cell-tags">
                    <span class="tag tag-kind">京町家</span>
                  </div>
                  <h3 class="facility-cell-name">天界</h3>
                  <p class="facility-relate-cell-prefecture">京都府・京都の東山の歴史豊かな地域の真ん中に位置。</p>
                  <p class="facility-relate-cell-lowest-price"><strong>30,000円〜</strong>（税抜）</p>
                </div>
              </figure>
            </a>
          </li>
          <li id="ember2274" class="ember-view facility-relate-cell">
            <a id="ember2275" href="/facilities/72" class="ember-view">
              <figure id="facility-72" class="facility-relate-cell-figure facility-cell-kyomachiya">
                <div class="facility-relate-cell-image-wrapper" style="background-image:url(https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/72/large_large_img_3941.jpg)">
                  <img src="https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/72/large_large_img_3941.jpg">
                </div>
                <div class="facility-relate-cell-details">
                  <div class="facility-cell-tags">
                    <span class="tag tag-kind">京町家</span>
                  </div>
                  <h3 class="facility-cell-name">京の片泊まり　清水びろうど庵</h3>
                  <p class="facility-relate-cell-prefecture">京都府・大正時代の情緒を色濃く残す京町屋貸切宿。</p>
                  <p class="facility-relate-cell-lowest-price"><strong>26,852円〜</strong>（税抜）</p>
                </div>
              </figure>
            </a>
          </li>
          <li id="ember2276" class="ember-view facility-relate-cell">
            <a id="ember2277" href="/facilities/38" class="ember-view">
              <figure id="facility-38" class="facility-relate-cell-figure facility-cell-shukubo">
                <div class="facility-relate-cell-image-wrapper" style="background-image:url(https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/38/large_file.jpg)">
                  <img src="https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/38/large_file.jpg">
                </div>
                <div class="facility-relate-cell-details">
                  <div class="facility-cell-tags">
                    <span class="tag tag-kind">宿坊</span>
                  </div>
                  <h3 class="facility-cell-name">知恩院　和順会館</h3>
                  <p class="facility-relate-cell-prefecture">京都府・浄土宗総本山知恩院の宿坊</p>
                  <p class="facility-relate-cell-lowest-price"><strong>13,704円〜</strong>（税抜）</p>
                </div>
              </figure>
            </a>
          </li>
          <li id="ember2279" class="ember-view facility-relate-cell">
            <a id="ember2280" href="/facilities/44" class="ember-view">
              <figure id="facility-44" class="facility-relate-cell-figure facility-cell-kyomachiya">
                <div class="facility-relate-cell-image-wrapper" style="background-image:url(https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/44/large______.jpg)">
                  <img src="https://d33tm9vrc69bn6.cloudfront.net/uploads/facility/cover_image/44/large______.jpg">
                </div>
                <div class="facility-relate-cell-details">
                  <div class="facility-cell-tags">
                    <span class="tag tag-kind">京町家</span>
                  </div>
                  <h3 class="facility-cell-name">Guest house Connection 知恩院</h3>
                  <p class="facility-relate-cell-prefecture">京都府・祇園、八坂神社、知恩院。観光名所に囲まれた二階建て町家貸切宿。</p>
                  <p class="facility-relate-cell-lowest-price"><strong>23,148円〜</strong>（税抜）</p>
                </div>
              </figure>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>


  </div>
</main>
</div>





</article><!-- #post-## -->
