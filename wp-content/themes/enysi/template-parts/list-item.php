<?php
  if(get_the_post_thumbnail()) {
    $thumb_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumb01')[0];
    $eyecatch_code = '<img src="'.$thumb_image_url.'">';
  } else {
    if(post_custom('stay_main_instagram')){
      $eyecatch_code = '<div class="facility-cell-insta">'.post_custom('stay_main_instagram').'</div>';
    }else{
      $thumb_image_url = get_stylesheet_directory_uri().'/assets/images/common/ogp.jpg">';
      $eyecatch_code = '<img src="'.$thumb_image_url.'">';
    }
  };

  $low = $high = null; $low = $post->low; $high = $post->high;
  if( $low == $high ){
    $price_text = '<strong>'.number_format($low).'円</strong>（税込）';
  }else{
    $price_text = '<strong>'.number_format($low).'円〜'.number_format($high).'円</strong>（税抜）';
  }
?>


<li class="ember-view facility-cell">
  <a href="<?php the_permalink(); ?>" class="facility-cell-link-block ember-view">
    <div class="facility-cell-wrapper facility-cell-shukubo">
      <div class="facility-cell-image-wrapper" style="background-image:url(<?php echo $thumb_image_url; ?>)">
        <?php echo $eyecatch_code; ?>
      </div>
      <div class="facility-cell-thumbnails-wrapper">
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
      </div>
      <div class="facility-cell-details">
        <?php if( $terms = get_the_terms($post->ID, array('tags','area')) ) {
            echo '<div class="facility-cell-tags">';
          foreach ( $terms as $term ) {echo '<span class="tag ember-view" href="'.get_term_link($term).'">'.esc_html($term->name).'</span>';}
            echo '</div>';
          } ?>
        <h3 class="facility-cell-name"><?php the_title(); ?></h3>
        <?php if(post_custom('stay_text')){echo '<p class="facility-cell-headline">'.nl2br(esc_html(post_custom('stay_text'))).'</p>';} ?>
        <?php if(post_custom('stay_text')){echo '<p class="facility-cell-lowest-base-price">'.$price_text.'</p>';} ?>
        <p class="facility-cell-action"><a href="<?php the_permalink(); ?>" class="btn btn-block btn-primary ember-view">詳細ページを見る</a></p>
      </div>
    </div>
  </a>
</li>