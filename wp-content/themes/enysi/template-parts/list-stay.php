<?php
if(get_the_post_thumbnail()) {
  $thumb_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumb01')[0];
} else {
  $thumb_image_url = get_stylesheet_directory_uri().'/assets/images/common/ogp.jpg">';
};


        $rep_group = SCF::get( 'stay_price_group' );//リピートグループを代入
        if( $rep_group[0]['stay_price_name'])://グループ内１つ目の任意のフィールドに値があるか
          $price_high = $price_row = null;
          foreach ( $rep_group as $fields ) ://グループを繰り返し処理
            $price = intval( preg_replace('/[^0-9]/', '', $fields['stay_price_num']) );
            if(!$price_high){
              $price_high = $price; $price_row = $price;
            }
            if($price_high < $price) $price_high = $price;
            if($price_row > $price) $price_row = $price;
          endforeach;
          if($price_high == $price_row): $price_text = '<strong>'.number_format($price_high).'円</strong>（税抜）';
          else: $price_text = '<strong>'.number_format($price_row).'円〜'.number_format($price_high).'円</strong>（税抜）';
          endif;
        endif;

 ?>


<li class="ember-view facility-cell facility-list-insta">
  <div class="facility-cell-wrapper facility-cell-shukubo">
    <a href="<?php the_permalink(); ?>" class="facility-cell-link-block ember-view">
      <div class="facility-cell-image-wrapper" style="background-image:url(<?php echo $thumb_image_url; ?>)">
        <img src="<?php echo $thumb_image_url; ?>">
      </div>
      <div class="facility-cell-thumbnails-wrapper">
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
        <div class="thumbnail"><img class="cover" src="[object Object]"></div>
      </div>
    </a>
    <div class="facility-cell-details">
      <?php if ($terms = get_the_terms($post->ID, 'stay_tag')) {
          echo '<div class="facility-cell-tags">';
        foreach ( $terms as $term ) {echo '<a class="tag ember-view" href="'.get_term_link($term).'">'.esc_html($term->name).'</a>';}
          echo '</div>';
        } ?>
      <h3 class="facility-cell-name"><?php the_title(); ?></h3>
      <?php if(post_custom('stay_text')){echo '<p class="facility-cell-headline">'.nl2br(esc_html(post_custom('stay_text'))).'</p>';} ?>
      <?php if(post_custom('stay_text')){echo '<p class="facility-cell-lowest-base-price">'.$price_text.'</p>';} ?>
      <p class="facility-cell-action"><a id="ember1782" href="<?php the_permalink(); ?>" class="btn btn-block btn-primary ember-view">詳細ページを見る</a></p>
    </div>
  </div>
</li>