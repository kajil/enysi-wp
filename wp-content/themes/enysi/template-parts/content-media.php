<?php
  //if( is_single() && !is_user_logged_in() && !isBot() ) //個別記事 かつ ログインしていない かつ 非ボット
  set_post_views(); //アクセスをカウントする
?>
<div class="article__blog flex-left">


  <!-- facilities overview -->
  <div class="article__blog__overview overview">
    <header class="overview__header">
      <?php if( $terms = get_the_terms($post->ID, array('tags','area')) ) {
        echo '<p class="tags">';
        foreach ( $terms as $term ) {echo '<span class="tag tag--gray">'.esc_html($term->name).'</span>';}
        echo '</p>';
      } ?>
      <h1 class="mt15"><?php the_title(); ?></h1>
    </header>
    <div class="overview__description">
      <p class="date"><?php the_time('Y年n月j日') ?></p>
      <!-- share button -->
      <div class="article__share">
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
      </div>
    </div>

    <div class="overview__lead">
      <figure class="photo"><?php the_post_thumbnail('large'); ?></figure>
      <p class="text"><?php the_excerpt(); ?></p>
    </div>
  </div>

  <div class="article__blog__contents contents">
    <?php the_content(); ?>
  </div>


  <div class="article__blog__profile profile">
    <header class="profile__header">
      <h1 class="header--left">ライター情報</h1>
    </header>
    <div class="profile__info">
      <figure class="photo"><?php echo get_avatar(get_the_author_id(), 200); ?></figure>
      <div class="texts">
        <p class="name"><?php the_author_meta('display_name'); ?></p>
        <p class="text"><?php the_author_meta('description'); ?></p>
        <p class="link"><a href="<?php echo get_bloginfo('url').'/author/'.get_the_author_meta('user_nicename'); ?>">このライターの記事一覧</a></p>
      </div>
    </div>
  </div>






</div>