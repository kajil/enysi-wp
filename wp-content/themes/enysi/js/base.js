jQuery(function($) {

  /* header menu */
  $('a.gbtn').click(function() {
    $(this).toggleClass('active');
    $('#dnav').toggleClass('active');
    return false;
  });

  /* toggle button */
  $('.tgl-btn').click(function() {
    console.log('toggle');
    var targetId = $(this).attr('rel');
    $(this).toggleClass('tgl-active');
    $('#' + targetId).toggleClass('tgl-active');
    return false;
  });

  $('a[href*=\\#]').not('.noscr').click(function() {
    var href, position, speed, target;
    speed = 800;
    href = $(this).attr('href');
    target = $(href === '#' || href === '' ? 'html' : href);
    position = target.offset().top;
    $('body,html').animate({
      scrollTop: position
    }, speed, 'swing');
    return false;
  });

})
