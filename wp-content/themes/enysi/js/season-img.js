jQuery(function($) {

	//72更新

	if($('#season72').length){
		var date = new Date();
		var year = date.getFullYear();
		var seasons = [
			{
				id  : 1,
				s04 : '冬',
				s24 : '冬至',
				s72 : '雪下出麦',
				txt1: '雪の下で、',
				txt2: '麦が芽を出すころ',
				sday: new Date(year,0,1,0,0,0).getTime()
			},
			{
				id  : 2,
				s04 : '冬',
				s24 : '小寒',
				s72 : '芹乃栄',
				txt1: '水辺で芹が',
				txt2: '盛んに育つころ',
				sday: new Date(year,0,5,0,0,0).getTime()
			},
			{
				id  : 3,
				s04 : '冬',
				s24 : '小寒',
				s72 : '水泉動',
				txt1: '地中で凍った水が、',
				txt2: '暖かくなり動き始めるころ',
				sday: new Date(year,0,10,0,0,0).getTime()
			},
			{
				id  : 4,
				s04 : '冬',
				s24 : '小寒',
				s72 : '雉始鳴',
				txt1: '雉が妻を恋いて、',
				txt2: '麦が芽なくころを出すころ',
				sday: new Date(year,0,15,0,0,0).getTime()
			},
			{
				id  : 5,
				s04 : '冬',
				s24 : '大寒',
				s72 : '款冬華',
				txt1: '雪の間からふきの花が',
				txt2: '咲きはじめるころ',
				sday: new Date(year,0,20,0,0,0).getTime()
			},
			{
				id  : 6,
				s04 : '冬',
				s24 : '大寒',
				s72 : '水沢腹堅',
				txt1: '沢にながれる水さえも',
				txt2: '厚く固く張りつめるころ',
				sday: new Date(year,0,25,0,0,0).getTime()
			},
			{
				id  : 7,
				s04 : '冬',
				s24 : '大寒',
				s72 : '雞始乳',
				txt1: 'にわとりが鳥屋で',
				txt2: '卵を産み始めるころ',
				sday: new Date(year,0,30,0,0,0).getTime()
			},


			{
				id  : 8,
				s04 : '春',
				s24 : '立春',
				s72 : '東風解凍',
				txt1: '東風が吹き、',
				txt2: '厚い氷を解かし始める',
				sday: new Date(year,1,4,0,0,0).getTime()
			},
			{
				id  : 9,
				s04 : '春',
				s24 : '立春',
				s72 : '黄鶯睍睆',
				txt1: '春告げ鳥の鶯が、',
				txt2: '梅の枝で鳴きはじめるころ',
				sday: new Date(year,1,9,0,0,0).getTime()
			},
			{
				id  : 10,
				s04 : '春',
				s24 : '立春',
				s72 : '魚上氷',
				txt1: '春の暖かさで湖や川の氷が割れ、',
				txt2: '公魚が水上に跳ね上がる頃',
				sday: new Date(year,1,14,0,0,0).getTime()
			},
			{
				id  : 11,
				s04 : '春',
				s24 : '雨水',
				s72 : '土脉潤起',
				txt1: '春の雨や雪解の水で大地が潤い',
				txt2: '命が宿るころ',
				sday: new Date(year,1,19,0,0,0).getTime()
			},
			{
				id  : 12,
				s04 : '春',
				s24 : '雨水',
				s72 : '霞始靆',
				txt1: '春霞が',
				txt2: 'たなびき始める頃',
				sday: new Date(year,1,24,0,0,0).getTime()
			},
			{
				id  : 13,
				s04 : '春',
				s24 : '雨水',
				s72 : '草木萠動',
				txt1: '草木が',
				txt2: '芽吹くころ',
				sday: new Date(year,2,1,0,0,0).getTime()
			},
			{
				id  : 14,
				s04 : '春',
				s24 : '啓蟄',
				s72 : '蟄虫啓戸',
				txt1: '地中まで春の日差しが届き',
				txt2: '虫が姿を現すころ',
				sday: new Date(year,2,6,0,0,0).getTime()
			},
			{
				id  : 15,
				s04 : '春',
				s24 : '啓蟄',
				s72 : '桃始笑',
				txt1: '桃の花がほころび',
				txt2: '青き踏むころ',
				sday: new Date(year,2,11,0,0,0).getTime()
			},
			{
				id  : 16,
				s04 : '春',
				s24 : '啓蟄',
				s72 : '菜虫化蝶',
				txt1: '初蝶が空を舞う',
				txt2: '心地良いころ',
				sday: new Date(year,2,16,0,0,0).getTime()
			},
			{
				id  : 17,
				s04 : '春',
				s24 : '春分',
				s72 : '雀始巣',
				txt1: '雀は巣作りをはじめ、',
				txt2: '物種蒔くころ',
				sday: new Date(year,2,21,0,0,0).getTime()
			},
			{
				id  : 18,
				s04 : '春',
				s24 : '春分',
				s72 : '桜始開',
				txt1: '桜が咲きはじめ',
				txt2: '豊作を告げるころ',
				sday: new Date(year,2,26,0,0,0).getTime()
			},
			{
				id  : 19,
				s04 : '春',
				s24 : '春分',
				s72 : '雷乃発声',
				txt1: '春雷が',
				txt2: '春の訪れを告げるころ',
				sday: new Date(year,2,31,0,0,0).getTime()
			},
			{
				id  : 20,
				s04 : '春',
				s24 : '清明',
				s72 : '玄鳥至',
				txt1: '燕が南国から渡り',
				txt2: '春の到来を告げるころ',
				sday: new Date(year,3,5,0,0,0).getTime()
			},
			{
				id  : 21,
				s04 : '春',
				s24 : '清明',
				s72 : '鴻雁北',
				txt1: '雁が隊をつらねて',
				txt2: '雲間はるかに消えて行くころ',
				sday: new Date(year,3,10,0,0,0).getTime()
			},
			{
				id  : 22,
				s04 : '春',
				s24 : '清明',
				s72 : '虹始見',
				txt1: '雨上がりに淡く',
				txt2: '春の虹が姿を現すころ',
				sday: new Date(year,3,15,0,0,0).getTime()
			},
			{
				id  : 23,
				s04 : '春',
				s24 : '穀雨',
				s72 : '葭始生',
				txt1: '水辺の葦が',
				txt2: '芽を吹きはじめるころ',
				sday: new Date(year,3,20,0,0,0).getTime()
			},
			{
				id  : 24,
				s04 : '春',
				s24 : '穀雨',
				s72 : '霜止出苗',
				txt1: '霜が終わり',
				txt2: '苗が生長するころ',
				sday: new Date(year,3,25,0,0,0).getTime()
			},
			{
				id  : 25,
				s04 : '春',
				s24 : '穀雨',
				s72 : '牡丹華',
				txt1: '牡丹が',
				txt2: '大きな花を咲かせるころ',
				sday: new Date(year,3,30,0,0,0).getTime()
			},


			{
				id  : 26,
				s04 : '夏',
				s24 : '立夏',
				s72 : '鼃始鳴',
				txt1: '恋の季節となり、',
				txt2: '雨蛙が鳴き始めるころ',
				sday: new Date(year,4,5,0,0,0).getTime()
			},
			{
				id  : 27,
				s04 : '夏',
				s24 : '立夏',
				s72 : '蚯蚓出',
				txt1: '冬眠していた蚯蚓が',
				txt2: '地上に這出るころ',
				sday: new Date(year,4,10,0,0,0).getTime()
			},
			{
				id  : 28,
				s04 : '夏',
				s24 : '立夏',
				s72 : '竹笋生',
				txt1: '筍が一気に',
				txt2: '成長するころ',
				sday: new Date(year,4,15,0,0,0).getTime()
			},
			{
				id  : 29,
				s04 : '夏',
				s24 : '小満',
				s72 : '蚕起食桑',
				txt1: '蚕が孵化し、',
				txt2: '生い茂った桑の葉を食べ始めるころ',
				sday: new Date(year,4,20,0,0,0).getTime()
			},
			{
				id  : 30,
				s04 : '夏',
				s24 : '小満',
				s72 : '紅花栄',
				txt1: '紅花の鮮やかな花が',
				txt2: '咲かんに咲き誇るころ',
				sday: new Date(year,4,25,0,0,0).getTime()
			},
			{
				id  : 31,
				s04 : '夏',
				s24 : '小満',
				s72 : '麦秋至',
				txt1: '麦が熟して',
				txt2: '実るころ',
				sday: new Date(year,4,30,0,0,0).getTime()
			},
			{
				id  : 32,
				s04 : '夏',
				s24 : '小満',
				s72 : '螳螂生',
				txt1: '孵化したカマキリが',
				txt2: '一斉にでてくるころ',
				sday: new Date(year,5,6,0,0,0).getTime()
			},
			{
				id  : 33,
				s04 : '夏',
				s24 : '小満',
				s72 : '腐草為蛍',
				txt1: '草が蒸れ、',
				txt2: '蛍となり光を放つころ',
				sday: new Date(year,5,11,0,0,0).getTime()
			},
			{
				id  : 34,
				s04 : '夏',
				s24 : '小満',
				s72 : '梅子黄',
				txt1: '青く太った梅の実が',
				txt2: '熟して色づくころ',
				sday: new Date(year,5,16,0,0,0).getTime()
			},
			{
				id  : 35,
				s04 : '夏',
				s24 : '夏至',
				s72 : '乃東枯',
				txt1: '夏枯草が',
				txt2: '枯れるころ',
				sday: new Date(year,5,21,0,0,0).getTime()
			},
			{
				id  : 36,
				s04 : '夏',
				s24 : '夏至',
				s72 : '菖蒲華',
				txt1: '菖蒲や燕子花の花が',
				txt2: '咲きはじめるころ',
				sday: new Date(year,5,27,0,0,0).getTime()
			},
			{
				id  : 37,
				s04 : '夏',
				s24 : '夏至',
				s72 : '半夏生',
				txt1: '烏柄杓が',
				txt2: '生えるころ',
				sday: new Date(year,6,2,0,0,0).getTime()
			},
			{
				id  : 38,
				s04 : '夏',
				s24 : '小暑',
				s72 : '温風至',
				txt1: '暖かい南風が',
				txt2: '吹くころ',
				sday: new Date(year,6,7,0,0,0).getTime()
			},
			{
				id  : 39,
				s04 : '夏',
				s24 : '小暑',
				s72 : '蓮始開',
				txt1: '蓮の花が開きはじめ',
				txt2: '蓮見をたのしむころ',
				sday: new Date(year,6,12,0,0,0).getTime()
			},
			{
				id  : 40,
				s04 : '夏',
				s24 : '小暑',
				s72 : '鷹乃学習',
				txt1: '鷹の幼鳥が飛ぶことを覚え',
				txt2: '巣立ちを迎えるころ',
				sday: new Date(year,6,17,0,0,0).getTime()
			},
			{
				id  : 41,
				s04 : '夏',
				s24 : '大暑',
				s72 : '桐始結花',
				txt1: '梢に高く桐の花が',
				txt2: '実をつけるころ',
				sday: new Date(year,6,23,0,0,0).getTime()
			},
			{
				id  : 42,
				s04 : '夏',
				s24 : '大暑',
				s72 : '土潤溽暑',
				txt1: '土中の水分が蒸発し、',
				txt2: '蒸し暑いころ',
				sday: new Date(year,6,29,0,0,0).getTime()
			},
			{
				id  : 43,
				s04 : '夏',
				s24 : '大暑',
				s72 : '大雨時行',
				txt1: '夕立が打ち水のように',
				txt2: '暑気を冷ますころ',
				sday: new Date(year,7,3,0,0,0).getTime()
			},


			{
				id  : 44,
				s04 : '秋',
				s24 : '立秋',
				s72 : '涼風至',
				txt1: '暑さの中に涼しい風が',
				txt2: '立ち始めるころ',
				sday: new Date(year,7,7,0,0,0).getTime()
			},
			{
				id  : 45,
				s04 : '秋',
				s24 : '立秋',
				s72 : '寒蝉鳴',
				txt1: 'ヒグラシが鳴き、',
				txt2: '秋の訪れを告げるころ',
				sday: new Date(year,7,13,0,0,0).getTime()
			},
			{
				id  : 46,
				s04 : '秋',
				s24 : '立秋',
				s72 : '蒙霧升降',
				txt1: '深い霧がまとわりつくように',
				txt2: '立ち籠めるころ',
				sday: new Date(year,7,18,0,0,0).getTime()
			},
			{
				id  : 47,
				s04 : '秋',
				s24 : '立秋',
				s72 : '綿柎開',
				txt1: '綿を包むガクが',
				txt2: '開き始めるころ',
				sday: new Date(year,7,23,0,0,0).getTime()
			},
			{
				id  : 48,
				s04 : '秋',
				s24 : '立秋',
				s72 : '天地始粛',
				txt1: '天地の暑さが鎮まり、',
				txt2: '涼しくなり始めるころ',
				sday: new Date(year,7,28,0,0,0).getTime()
			},
			{
				id  : 49,
				s04 : '秋',
				s24 : '立秋',
				s72 : '禾乃登',
				txt1: '稲が実り、',
				txt2: '黄金色の穂をたらすころ',
				sday: new Date(year,8,2,0,0,0).getTime()
			},
			{
				id  : 50,
				s04 : '秋',
				s24 : '立秋',
				s72 : '草露白',
				txt1: '草に降りた霜が、',
				txt2: '白く光って見えるころ',
				sday: new Date(year,8,8,0,0,0).getTime()
			},
			{
				id  : 51,
				s04 : '秋',
				s24 : '立秋',
				s72 : '鶺鴒鳴',
				txt1: 'セキレイが尾で地面を叩き',
				txt2: '鳴き始めるころ',
				sday: new Date(year,8,13,0,0,0).getTime()
			},
			{
				id  : 52,
				s04 : '秋',
				s24 : '立秋',
				s72 : '玄鳥去',
				txt1: '燕が子育てを終え、',
				txt2: '南の国へだんだんに帰るころ',
				sday: new Date(year,8,18,0,0,0).getTime()
			},
			{
				id  : 53,
				s04 : '秋',
				s24 : '立秋',
				s72 : '雷乃収声',
				txt1: '雷が声を',
				txt2: 'ひそめはじめるころ',
				sday: new Date(year,8,23,0,0,0).getTime()
			},
			{
				id  : 54,
				s04 : '秋',
				s24 : '立秋',
				s72 : '蟄虫坏戸',
				txt1: '虫が土にもぐって穴を塞ぎ',
				txt2: '冬ごもりの支度をはじめるころ',
				sday: new Date(year,8,28,0,0,0).getTime()
			},
			{
				id  : 55,
				s04 : '秋',
				s24 : '立秋',
				s72 : '水始涸',
				txt1: '稲穂が実り',
				txt2: '田を干しはじめるころ',
				sday: new Date(year,9,3,0,0,0).getTime()
			},
			{
				id  : 56,
				s04 : '秋',
				s24 : '立秋',
				s72 : '鴻雁来',
				txt1: '雁が飛来し',
				txt2: 'はじめるころ',
				sday: new Date(year,9,8,0,0,0).getTime()
			},
			{
				id  : 57,
				s04 : '秋',
				s24 : '立秋',
				s72 : '菊花開',
				txt1: '寒さに負けず、',
				txt2: '菊が咲きはじめるころ',
				sday: new Date(year,9,13,0,0,0).getTime()
			},
			{
				id  : 58,
				s04 : '秋',
				s24 : '立秋',
				s72 : '蟋蟀在戸',
				txt1: '戸のあたりや軒先で、',
				txt2: '秋の虫の声が聞こえはじめるころ',
				sday: new Date(year,9,18,0,0,0).getTime()
			},
			{
				id  : 59,
				s04 : '秋',
				s24 : '立秋',
				s72 : '霜始降',
				txt1: '朝夕が冷えこみ、',
				txt2: '霜が降りはじめるころ',
				sday: new Date(year,9,23,0,0,0).getTime()
			},
			{
				id  : 60,
				s04 : '秋',
				s24 : '立秋',
				s72 : '霎時施',
				txt1: '小雨がしとしと降ってはやみ',
				txt2: '一雨ごとに気温が下がってゆくころ',
				sday: new Date(year,9,28,0,0,0).getTime()
			},
			{
				id  : 61,
				s04 : '秋',
				s24 : '立秋',
				s72 : '楓蔦黄',
				txt1: '紅葉前線が日本列島を染め',
				txt2: '秋の終わりを告げるころ',
				sday: new Date(year,10,2,0,0,0).getTime()
			},


			{
				id  : 62,
				s04 : '冬',
				s24 : '立冬',
				s72 : '山茶始開',
				txt1: 'サザンカの花が冬の始まりを',
				txt2: '告げるように咲き始めるころ',
				sday: new Date(year,10,7,0,0,0).getTime()
			},
			{
				id  : 63,
				s04 : '冬',
				s24 : '立冬',
				s72 : '地始凍',
				txt1: '夜の冷え込みが厳しく、',
				txt2: '大地が固く凍りはじめるころ',
				sday: new Date(year,10,12,0,0,0).getTime()
			},
			{
				id  : 64,
				s04 : '冬',
				s24 : '立冬',
				s72 : '金盞香',
				txt1: '水仙の花が',
				txt2: '芳香を放つころ',
				sday: new Date(year,10,17,0,0,0).getTime()
			},
			{
				id  : 65,
				s04 : '冬',
				s24 : '小雪',
				s72 : '虹蔵不見',
				txt1: '陽の光も弱まり、',
				txt2: '虹が見えなくなるころ',
				sday: new Date(year,10,22,0,0,0).getTime()
			},
			{
				id  : 66,
				s04 : '冬',
				s24 : '小雪',
				s72 : '朔風払葉',
				txt1: '北風が木の葉を',
				txt2: '吹き飛ばすころ',
				sday: new Date(year,10,27,0,0,0).getTime()
			},
			{
				id  : 67,
				s04 : '冬',
				s24 : '小雪',
				s72 : '橘始黄',
				txt1: '橘の実が',
				txt2: '黄色く色付くころ',
				sday: new Date(year,11,2,0,0,0).getTime()
			},
			{
				id  : 68,
				s04 : '冬',
				s24 : '大雪',
				s72 : '閉塞成冬',
				txt1: '天地の気がふさがり',
				txt2: '真冬となるころ',
				sday: new Date(year,11,7,0,0,0).getTime()
			},
			{
				id  : 69,
				s04 : '冬',
				s24 : '大雪',
				s72 : '熊蟄穴',
				txt1: '脂肪を溜め込んだ熊が',
				txt2: '冬眠の為に穴に籠るころ',
				sday: new Date(year,11,12,0,0,0).getTime()
			},
			{
				id  : 70,
				s04 : '冬',
				s24 : '大雪',
				s72 : '鱖魚群',
				txt1: '鮭が群れをなして',
				txt2: '故郷の川を上るころ',
				sday: new Date(year,11,16,0,0,0).getTime()
			},
			{
				id  : 71,
				s04 : '冬',
				s24 : '冬至',
				s72 : '乃東生',
				txt1: '夏枯草が',
				txt2: '芽を出すころ',
				sday: new Date(year,11,22,0,0,0).getTime()
			},
			{
				id  : 72,
				s04 : '冬',
				s24 : '冬至',
				s72 : '麋角解',
				txt1: '大鹿の古い角が',
				txt2: '落ちるころ',
				sday: new Date(year,11,27,0,0,0).getTime()
			},
		]

		setData = function(setSeason) {
			$('#season72-s04').text(setSeason.s04);
			$('#season72-s24').text(setSeason.s24);
			$('#season72-s72').text(setSeason.s72);
			$('#season72-txt1').text(setSeason.txt1);
			$('#season72-txt2').text(setSeason.txt2);
			var prefix = ($(window).width() > 750) ? '' : 'sp_';
			var bgimg = '/wp-content/themes/enysi/assets/images/top/' + prefix + setSeason.s24 + '.png'
			$('#season72').css('background-image', 'url(' + bgimg + ')');

			return false;
		};

		getData = function() {
			var today = new Date().getTime();

			for (i = 0; i < seasons.length; i ++) {
				if(today >= parseInt(seasons[i].sday)) {
					curMonth = i;
				}else{
					setData(seasons[i-1]);
					return false;
				}
			}
		};
		getData();

	}
})

