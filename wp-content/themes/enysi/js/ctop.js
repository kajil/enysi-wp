// (function () {
( function( $ ) {
    // $(function () {
        var n,
            t,
            s;
        if ($("#wtop").hasClass("page_category")) 
            return t = function () {
                var n,
                    t,
                    s,
                    i,
                    o,
                    r,
                    a,
                    l;
                return o = Math.PI / 180 * 8,
                l        = winW * Math.tan(o),
                $(".story").css({
                    marginTop: -1 *l + "px"
                }),
                r = 970 * winW / 1280,
                $(".story").css({
                    height: r + "px"
                }),
                $(".story.last .in dl dd").css({
                    height: r + "px"
                }),
                a = 820 * winW / 1280,
                $(".story.min").css({
                    height: a + "px"
                }),
                $("#wtop").hasClass("lang_fr") && (
                    s = 970 * winW / 1280 - l,
                    i = 830 * winW / 1280 - l,
                    n = $(".story:not(.last) .in dl"),
                    t = $(".story.min:not(.last) .in dl"),
                    n.css("padding-top", s + "px"),
                    t.css("padding-top", i + "px")
                ),
                !1
            }
        ,
        n = function () {
            var n,
                t,
                s,
                i,
                o,
                r,
                a;
            for (
                r     = $(".story"),
                a     = $("#nav li a"),
                i     = $win.scrollTop(),
                n     = t = 0,
                o     = r.length - 1;
                t <= o;
                n = t += 1
            ) 
                i + 200 > $(r[n])
                    .offset()
                    .top && (s = n);
            return $(a).removeClass("on"),
            $(a[s]).addClass("on")
        },
        function () {
            var n,
                t;
            return t = $("#bgvid")[0],
            n        = $win.scrollTop(),
            n + 100 < $(t)
                .offset()
                .top
                    ? t.pause()
                    : t.play()
        },
        s = function () {
            var n,
                t,
                s,
                i,
                o,
                r,
                a,
                l;
            return a = Math.PI / 180 * 8,
            l        = winW * Math.tan(a) * 2,
            i        = 920 * winW / 750 - l,
            o        = 734 * winW / 750 - l,
            r        = 920 * winW / 750,
            n        = $(".story:not(.last) .in dl dt"),
            t        = $(".story.min:not(.last) .in dl dt"),
            s        = $(".story.last .in dl"),
            n.css("margin-top", i + "px"),
            t.css("margin-top", o + "px"),
            s.css("height", r + "px")
        },
        winW > breakpoint
            ? (t(), n())
            : s(),
        $win
            .on("resize", function () {
                return winW > breakpoint
                    ? (t(), n())
                    : s()
            })
            .on("scroll", function () {
                return n()
            })
    })
// }).call(this);