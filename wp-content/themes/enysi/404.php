<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package enysi
 */

get_header(); ?>

<div id="ember740" class="ember-view wrap"><main>
  <div class="container">

<!---->
      <section class="content"><div class="in">
  <p class="error_message">ご指定のページが見つかりませんでした。<br>
  404 Page Not found</p>
  <p class="back_to_home"><a id="ember741" href="/" class="ember-view">HOMEへ戻る</a></p>
</div></section>


  </div>
</main>
</div>

<?php
get_footer();
