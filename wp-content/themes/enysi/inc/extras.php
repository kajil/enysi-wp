<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package enysi
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function enysi_body_classes( $classes ) {
  // Adds a class of group-blog to blogs with more than 1 published author.
  if ( is_multi_author() ) {
    $classes[] = 'group-blog';
  }

  // Adds a class of hfeed to non-singular pages.
  if ( ! is_singular() ) {
    $classes[] = 'hfeed';
  }

  return $classes;
}
add_filter( 'body_class', 'enysi_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function enysi_pingback_header() {
  if ( is_singular() && pings_open() ) {
    echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
  }
}
add_action( 'wp_head', 'enysi_pingback_header' );


/* 固定ページのみ自動整形機能を無効化 */
function disable_page_wpautop() {
  if ( is_page() ) remove_filter( 'the_content', 'wpautop' );
}
add_action( 'wp', 'disable_page_wpautop' );


/* Smart Custom Fields */
SCF::add_options_page( 'サイト設定', 'サイト設定', 'edit_pages', 'site-options' );


//管理画面項目の名称変更
function edit_admin_menus() {
    global $menu;
    global $submenu;
    //投稿
    $menu[5][0] = 'NEWS';
    $submenu['edit.php'][5][0] = 'NEWS 一覧';
    //メディア
    $menu[10][0] = 'ファイル';
    $submenu['upload.php'][5][0] = 'ファイルライブラリ';
}
add_action('admin_menu', 'edit_admin_menus');

function change_post_object_label() {
  global $wp_post_types;
  //投稿
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'NEWS';
  $labels->singular_name = 'NEWS';
  $labels->add_new = _x('追加', 'NEWS');
  $labels->add_new_item = 'NEWSの新規追加';
  $labels->edit_item = 'NEWSの編集';
  $labels->new_item = '新規NEWS';
  $labels->view_item = 'NEWSを表示';
  $labels->search_items = 'NEWSを検索';
  $labels->not_found = '記事が見つかりませんでした';
  $labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
  //メディア
  $labels = &$wp_post_types['attachment']->labels;
  $labels->name = 'ファイル';
  $labels->singular_name = 'ファイル';
  $labels->edit_item = 'ファイルを編集';
  $labels->all_items = 'ファイル';
  $labels->archives = 'ファイル';
  $labels->menu_name = 'ファイル';
  $labels->name_admin_bar = 'ファイル';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );



/* 宿泊施設登録・更新時にカスタムフィールドに金額のlowとhighを追加
*************************************************************** */
add_action( 'save_post' , 'action_save_post', 99, 2 );
function action_save_post( $post_id, $post ) {
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {// 自動保存時
    return $post_id;// 何もしない
  } elseif ( !empty( $_POST ) ) {// 投稿更新時



    $rep_group = SCF::get( 'stay_price_group' );
    if( $rep_group[0]['stay_price_name']):
      $i = 0;
      foreach ( $rep_group as $fields ) :

        $price = intval( preg_replace('/[^0-9]/', '', $fields['stay_price_num']) );

        if($i==0)://最初だけ代入

          if($price>0):
            $high = $price;
            $low = $price;
          endif;

        else:

          if($price>0):
            if($high < $price) $high = $price;//数字が大きかったら代入
            if($low > $price) $low = $price;//数字が小さかったら代入
          endif;

        endif;
        $i++;
      endforeach;
    endif;


    update_post_meta( $post_id, 'low',  $low );
    update_post_meta( $post_id, 'high', $high );
  }
}

