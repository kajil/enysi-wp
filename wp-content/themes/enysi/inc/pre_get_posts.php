<?php
/* pre_get_posts */
function customize_main_query ( $query ) {
  if ( ! is_admin() && $query->is_main_query() ) { // 管理画面以外 かつ メインクエリー
    if ( $query->is_tax() ) {
      $query->set( 'posts_per_page', '20' );
    }
    if ( $query->is_author() ) {
      $query->set( 'post_type', 'media' );
      $query->set( 'posts_per_page', '20' );
    }
    // if ( $query->is_post_type_archive('stay') ) {
    //   $query->set( 'posts_per_page', '30' );
    //   $query->set( 'orderby', 'date' );
    //   $query->set( 'order', 'DESC' );
    // }
  }
}
add_action( 'pre_get_posts', 'customize_main_query' ); // pre_get_postsにフック
