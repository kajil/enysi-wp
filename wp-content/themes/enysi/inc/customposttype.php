<?php

/*
 * カスタム投稿タイプ & カスタムタクソノミー
 */

/* カスタム投稿タイプ
*************************************************************** */
add_action( 'init', 'my_post_type' );
function my_post_type() {

  register_post_type(
    'stay',
    array(
      $labels = array(
        'name' => '宿泊',
        'singular_name' => '宿泊施設',
        'menu_name' => '宿泊'
      ),
      'labels' => $labels,
      'menu_position' => 5,
      'has_archive' => true,
      'show_ui' => true,
      'public' => true,
      'supports' => array('title', 'thumbnail', 'excerpt'),
    )
  );

  register_post_type(
    'media',
    array(
      $labels = array(
        'name' => 'メディア',
        'singular_name' => 'メディア',
        'menu_name' => 'メディア'
      ),
      'labels' => $labels,
      'menu_position' => 5,
      'has_archive' => true,
      'show_ui' => true,
      'public' => true,
      'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
    )
  );

};



/* CustomTaxonomy
*************************************************************** */
add_action('init', 'register_custom_post');
function register_custom_post() {

  $tag_args = array(
    'label' => 'エリア',
    'labels' => array(
      'name' => 'エリア',
      'singular_name' => 'エリア',
    ),
    'public' => true,
    'show_ui' => true,
    'hierarchical' => true,
  );
  register_taxonomy('area' , array('stay','media'), $tag_args);

  $tag_args = array(
    'label' => 'コンテンツタグ',
    'labels' => array(
      'name' => 'コンテンツタグ',
      'singular_name' => 'コンテンツタグ',
    ),
    'public' => true,
    'show_ui' => true,
    'hierarchical' => true,
  );
  register_taxonomy('tags' , array('stay','media'), $tag_args);

};
