<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package enysi
 */

?>
<!DOCTYPE html prefix="og: http://ogp.me/ns#">
<?php if ( is_page(array(2,588)) || $post->post_parent === 2 ) {
  $html_class = 'enysi-brand';
} else {
  $html_class = 'enysi-main';
} ?>
<html lang="ja" class="<?php echo $html_class; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<?php wp_head(); ?>

    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/apple-touch-icon.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.png">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,500" type="text/css">
    <link rel="canonical" href="<?php echo ( 'https://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ] ); ?>">

    <?php if( is_single() ) { ?>
    <meta property="og:type" content="article">
    <meta property="og:description" content="<?php echo mb_substr( str_replace( array( "\r\n" , "\n" , "\r" ), '', strip_tags( $post->post_content ) ), 0, 100 ); ?>">
    <?php } else { ?>
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?php bloginfo( 'description' ); ?>">
    <?php } ?>
    
    <?php if( ( is_single() || is_page() ) && has_post_thumbnail() ) { ?>
    <meta property="og:image" content="<?php the_post_thumbnail_url( 'full' ); ?>">
    <?php } else { ?>
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/common/ogp.png">
    <?php } ?>

    <meta property="og:title" content="<?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?>">
    <meta property="og:url" content="<?php echo ( 'https://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ] ); ?>">
    <meta property="fb:app_id" content="314733888984011">

    <!-- {{content-for "head-footer"}} -->

    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-THZBKV');</script>
    <!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THZBKV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="ember-view global">
  <div class="in">
    <h1 class="logo">
      <a href="/" class="ember-view active">
        <svg class="enysi-logo">
          <image width="100%" height="100%" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/common/enysi-logo.svg" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/enysi-logo-header.png" alt="ENYSi"></image>
        </svg>
      </a>
    </h1>
    <a class="gbtn _active" href="javascript:void(0);">
      <span></span>
      <span></span>
      <span></span>
    </a>
  </div>
  <nav class="_active" id="dnav">
    <?php
      $args = array(
        'theme_location' => 'global_navigation',
        'menu_id' => 'global_navigation',
        'container' => false,
        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
      );
      wp_nav_menu($args);
    ?>
  </nav>
</header>