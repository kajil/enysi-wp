<div class="article__parts flex-right">


  <?php
    $pup = SCF::get_option_meta( 'site-options', 'pickup_post' );
    if( $pup[0]):

      echo '<div class="article__parts__ranking ranking">';
      echo '<header class="header header--left"><h1>ピックアップ</h1></header>';
      echo '<div class="ranking__list">';
      $args = array(
        'include' => $pup,
        'post_type' => 'any',
      );
      $posts = get_posts( $args );
      foreach ( $posts as $post ) :
        setup_postdata( $post );
        get_template_part( 'template-parts/list', 'sidebar' );
      endforeach;
      echo '</div></div>';

    endif;
  ?>


  <?php

    $args = array(
      'post_type'     => array('stay','media'),
      'numberposts'   => 5,       //表示数
      'meta_key'      => 'pv_count',
      'orderby'       => 'meta_value_num',
      'order'         => 'DESC',
    );
    $posts = get_posts( $args );
    if( $posts ):

      echo '<div class="article__parts__ranking ranking">';
      echo '<header class="header header--left"><h1>人気の記事</h1></header>';
      echo '<div class="ranking__list">';

      global $i; $i = null;
      foreach ( $posts as $post ) :
        setup_postdata( $post );
        $i++;
        get_template_part( 'template-parts/list', 'sidebar' );
      endforeach;
      $i = null;

      echo '</div></div>';

    else :
      echo '<p>ランキングはまだ集計されていません。</p>';
    endif;
  ?>



</div>
