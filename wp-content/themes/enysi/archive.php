<?php get_header(); ?>

<div class="wrap">
  <main>
    <div class="container">
      <div class="facilities-index">

        <?php get_search_form(); ?>

        <section class="content">
          <div class="in">
            <header class="content-header">
              <?php
                if(is_post_type_archive()){
                  $post_type = get_query_var('post_type');
                  $pt = get_post_type_object($post_type);
                  $pt_title = $pt->labels->singular_name;
                }
               ?>
              <h1><?php echo $pt_title; ?>一覧</h1>
            </header>

            <ul class="facilities-grid">

              <?php if ( have_posts() ) : while( have_posts() ) : the_post();
                get_template_part( 'template-parts/list-item');
              endwhile; endif; ?>

            </ul>

          </div>
        </section>

        <?php get_template_part( 'template-parts/pager'); ?>

      </div>
    </div>
  </main>
</div>

<?php
get_footer();
