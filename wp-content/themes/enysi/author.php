<?php get_header(); ?>

<div id="ember755" class="ember-view wrap">
  <main>
    <div class="container">
      <div class="facilities-index">

        <section class="content">
          <div class="in">

            <header class="content-header">
              <div class="article__blog__profile profile">
                <div class="profile__info">
                  <figure class="photo"><?php echo get_avatar($author, 150); ?></figure>
                  <div class="texts">
                    <p class="name"><?php the_author_meta('display_name',$author); ?></p>
                    <p class="text"><?php the_author_meta('description',$author); ?></p>
                  </div>
                </div>
              </div>
            </header>

            <ul id="ember1773" class="ember-view facilities-grid">

              <?php if ( have_posts() ) : while( have_posts() ) : the_post();
                get_template_part( 'template-parts/list-item');
              endwhile; endif; ?>

            </ul>

          </div>
        </section>

        <?php get_template_part( 'template-parts/pager'); ?>

      </div>
    </div>
  </main>
</div>

<?php
get_footer();
