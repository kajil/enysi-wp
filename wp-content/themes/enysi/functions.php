<?php
/**
 * enysi functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package enysi
 */

if ( ! function_exists( 'enysi_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function enysi_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on enysi, use a find and replace
	 * to change 'enysi' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'enysi', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size('author_thumb',200,200,true);
	add_image_size('thumb01',860,520,true);
	add_image_size('thumb02',460,360,false);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'enysi' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'enysi_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'enysi_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function enysi_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'enysi_content_width', 640 );
}
add_action( 'after_setup_theme', 'enysi_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function enysi_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'enysi' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'enysi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'enysi_widgets_init' );


/* Custom Menu */
register_nav_menus(array(
  'global_navigation' => 'グローバルナビゲーション',
  'footer_sitemap01' => 'フッターサイトマップ1',
  'footer_sitemap02' => 'フッターサイトマップ2',
  'footer_navigation' => 'フッターナビゲーション',
));

/**
 * Enqueue scripts and styles.
 */
function enysi_scripts() {
	//wp_enqueue_style( 'enysi-style', get_stylesheet_uri() );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css',false,date('YmdHis', filemtime(get_template_directory() . '/style.css')),'all');
	wp_enqueue_style( 'vendor', get_template_directory_uri() . '/assets/css/vendor.css',false,date('YmdHis', filemtime(get_template_directory() . '/assets/css/vendor.css')),'all');
	wp_enqueue_style( 'frontend', get_template_directory_uri() . '/assets/css/enysi-frontend.css',false,date('YmdHis', filemtime(get_template_directory() . '/assets/css/enysi-frontend.css')),'all');

	wp_enqueue_script( 'enysi-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'base', get_template_directory_uri() . '/js/base.js', array(), '20170701', false );
	wp_enqueue_script( 'season-img', get_template_directory_uri() . '/js/season-img.js', array(), '20170701', false );
	wp_enqueue_script( 'ctop', get_template_directory_uri() . '/js/ctop.js', array(), false,date('YmdHis', filemtime(get_template_directory() . '/ctop.js')), true );
	wp_enqueue_script( 'jquery','https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array(), '1.11.3');

	wp_enqueue_script( 'enysi-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'enysi_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom post type and Custom taxonomy
 */
require get_template_directory() . '/inc/customposttype.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * shortcode.
 */
require get_template_directory() . '/inc/shortcode.php';

/**
 * Popular posts.
 */
require get_template_directory() . '/inc/popularposts.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/pre_get_posts.php';
