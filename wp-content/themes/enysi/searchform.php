<div class="searchs" id="searchs">
  <div class="in">
    <div class="searchi-btn">
      <a href="javascript:void(0);" class="btn btn-block btn-primary btn-search tgl-btn" rel="searchi-wrapper">詳細検索 <i class="fa fa-search" aria-hidden="true"></i></a>
    </div>
    <div class="searchi-wrapper" id="searchi-wrapper">
      <form class="search_form_wrapper" method="get" action="<?php echo home_url('/'); ?>">

        <p class="search-head">記事タイプ</p>
        <ul class="tag_group mb30">
          <li style="display:inline-block;padding-right:20px;">
            <label><input type="checkbox" name="select_pt[]" value="stay"> 旅の施設</label>
          </li>
          <li style="display:inline-block;padding-right:20px;">
            <label><input type="checkbox" name="select_pt[]" value="media"> 読み物</label>
          </li>
        </ul>

        <p class="search-head">フリーワード</p>
        <input name="s" id="s" class="mb30" placeholder="例）京都 整える" />

        <p class="search-head">エリア</p>
        <div class="tag_box mb30">
          <?php
            $taxonomy = 'area';
            $terms = get_terms( $taxonomy);
            if ( count( $terms ) != 0 ):
              echo '<ul class="tag_group">';
              foreach ( $terms as $term ):
                echo '<li style="display:inline-block;padding-right:20px;"><label><input type="checkbox" name="select_area[]" value="'.$term->slug.'"> '.$term->name.'</label></li>';
              endforeach;
              echo '</ul>';
            endif;
          ?>
        </div>

        <p class="search-head">タグ</p>
        <div class="tag_box mb30">
          <?php
            $taxonomy = 'tags';
            $terms = get_terms( $taxonomy);
            if ( count( $terms ) != 0 ):
              echo '<ul class="tag_group">';
              foreach ( $terms as $term ):
                echo '<li style="display:inline-block;padding-right:20px;"><label><input type="checkbox" name="select_tags[]" value="'.$term->slug.'"> '.$term->name.'</label></li>';
              endforeach;
              echo '</ul>';
            endif;
          ?>
        </div>

        <p class="search-head">料金</p>
        <div class="mb30">
          <label>価格帯（最小）:1000<input type="range" name="price_min" value="1000" min="1000" max="50000" step="1000" />50000</label><br>
          <label>価格帯（最大）:1000<input type="range" name="price_max" value="50000" min="1000" max="50000" step="1000" />50000</label>
        </div>

        <div class="searchi-btn">
          <input type="submit" class="btn btn-block btn-primary btn-search button02" value="検索">
        </div>


      </form>
    </div>
  </div>
</div>