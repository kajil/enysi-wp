( function( $ ) {

	'use strict';

	if ( typeof _flamingo === 'undefined' || _flamingo === null ) {
		return;
	}

	$( function() {
		postboxes.add_postbox_toggles( _flamingo.screenId );
	} );

} )( jQuery );
